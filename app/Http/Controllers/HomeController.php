<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Response;
use Session;
use App\register;
use App\state;
use App\city;

class HomeController extends Controller
{	
 
	public function __construct(Request $request,register $register,state $state,city $city)
	{
		$this->register = $register;
		$this->state = $state;
		$this->city = $city;
	}

	public function home(Request $request)
	{	
		$data = $this->register->getUserData();
		return view('home',compact('data'));
	}

}