<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Response;
use App\register;
use App\state;
use App\city;
use File;
use Hash;
use Session;
use Redirect; 

class LoginController extends Controller
{
	public function __construct(Request $request,register $register,state $state,city $city)
	{
		$this->register = $register;
		$this->state = $state;
		$this->city = $city;
	}

	public function login(Request $request)
	{	
		return view('login');
	}

	public function process_login(Request $request)
	{	
		$input = $request->all();
		$user = $this->register->getUser($input);
		if (Hash::check($request->get('password'), $user[0]->password)) {
			$userid=$user[0]->id;
            $name=$user[0]->first_name.' '.$user[0]->last_name;
            Session::put('userId', $userid);
            Session::put('name', $name);
            return "1"; 
            //Redirect('/admin/home');
		}else{
			return "2";
		}
		//return view('login');
	}

}