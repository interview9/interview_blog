<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Response;
use App\register;
use App\state;
use App\city;
use File;

class RegisterController extends Controller
{
	public function __construct(Request $request,register $register,state $state,city $city)
	{
		$this->register = $register;
		$this->state = $state;
		$this->city = $city;
	}

	public function register(Request $request)
	{	
		$state = $this->state->getState();
		$city = $this->city->getCity();
		return view('register',compact('state','city'));
	}

	public function add_register(Request $request)
	{
		$input = $request->all();
		$save_id = $this->register->saveRegister($input);
		$file = $request->file('profile');
            if ($file!="") {
                $file_name = $file->getClientOriginalName();
                $fileExtension = $file->getClientOriginalExtension();
                $file_name = explode(".", $file_name);
                $file_name = $this->randomName($file_name[0]);
                
            $new_file_name = $file_name.".".$fileExtension;

            $file->move(public_path('/profileimg'), $new_file_name); 
            
            $file_name = "profileimg/".$new_file_name;
            DB::table('register')
                ->where('id', $save_id)
                ->update(
                    [
                        'profile_path'=>$file_name
                    ]
                ); 
            }
            return 1;
	}

	public function randomName($name){
        $name = str_replace(" ", "-", $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
        $name = $name."-".time();
        return $name = preg_replace('/-+/', '-', $name);
    }

	public function get_city(Request $request)
	{
		$input = $request->all();
		$city = $this->city->getCityById($input);
		$html = '';
		$html .='<option>Please Select</option>';
		foreach ($city as $key => $value) {
			$html .='<option value="'.$value->id.'">'.$value->city_name.'</option>';
		}
		return $html;
	}

} 