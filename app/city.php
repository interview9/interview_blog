<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;

class city extends Model
{
	protected $table="city";
    protected $primaryKey = 'id';

    public function getCity()
    {
    	return DB::table('city')
    			->get()->toArray();
    }

    public function getCityById($input)
    {
    	return DB::table('city')
    			->where('state_id',$input['state'])
    			->get()->toArray();
    }

} 