<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;
use Hash;

class register extends Model
{
	protected $table="register";
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function saveRegister($input)
    {
    	$data = new register;
        $data->first_name = $input['fname'];
        $data->last_name = $input['lname'];
        $data->email = $input['email'];
        $data->mobile_no = $input['mobileno'];
        $data->state = $input['state'];
        $data->city = $input['city'];
        $data->password =  Hash::make($input['password']);
        //$data->profile_path = $input['profile'];
        $data->created_at = date('Y-m-d H:i:s');
        $data->updated_at = date('Y-m-d H:i:s');
        $data->save();
        if(!empty($data->id)){
            return $data->id;
        }else{
            return 'fail';
        }
    }

    public function getUser($input)
    {
    	return DB::table('register')
    			->where('email',$input['username'])
    			->get()->toArray();
    }

    public function getUserData()
    {
    	return DB::table('register as rs')
    			->join('state as st','st.id','rs.state')
    			->join('city as ct','ct.id','rs.city')
    			->get()->toArray();
    } 

}