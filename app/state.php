<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;

class state extends Model
{
	protected $table="state";
    protected $primaryKey = 'id';

    public function getState()
    {
    	return DB::table('state')
    			->get()->toArray();
    }

} 