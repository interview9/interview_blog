<h1>Welcome <?php echo Session::get('name'); ?>, you are logged in!!</h1>

<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile No</th>
                <th>State</th>
                <th>City</th>
                <th>Profile Picture</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody style="text-align: center;">
        	@foreach($data as $val)
            <tr>
                <td>{{$val->first_name.' '.$val->last_name}}</td>
                <td>{{$val->email}}</td>
                <td>{{$val->mobile_no}}</td>
                <td>{{$val->state_name}}</td>
                <td>{{$val->city_name}}</td>
                <td><img src="<?php echo url($val->profile_path); ?>" style="height:50px"></td>
                @if($val->status=='1')
                <td>Active</td>
                @else
                <td>InActive</td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table> 