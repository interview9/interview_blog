<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">


<div class="container">
<br><br>
<div class="row justify-content-center">
<div class="col-md-6">
<div class="card">
<header class="card-header">
  <a href="/login" class="float-right btn btn-outline-primary mt-1">Log in</a>
  <h4 class="card-title mt-2">Sign up</h4>
</header>
<article class="card-body">
<form id="registerFrm">
  {!! csrf_field() !!}
  <div class="form-row">
    <div class="col form-group">
      <label>First name </label>   
        <input type="text" name="fname" id="fname" class="form-control" placeholder="">
    </div> <!-- form-group end.// -->
    <div class="col form-group">
      <label>Last name</label>
        <input type="text" name="lname" id="lname" class="form-control" placeholder=" ">
    </div> <!-- form-group end.// -->
  </div> <!-- form-row end.// -->
  <div class="form-group">
    <label>Email address</label>
    <input type="email" class="form-control" name="email" id="email" placeholder="">
  </div> <!-- form-group end.// -->

  <div class="form-group">
    <label>Mobile No</label>
    <input type="text" class="form-control" name="mobileno" id="mobileno" placeholder="">
  </div>

  
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Select State</label>
      <select id="state" name="state" class="form-control">
        <option value=''> Please select </option>
        @foreach($state as $st)
          <option value={{$st->id}}> {{$st->state_name}} </option>
        @endforeach
      </select>
    </div> <!-- form-group end.// -->

    <div class="form-group col-md-6">
      <label>Select City</label>
      <select id="city" name="city" class="form-control">
        <option value=''> Please select </option>
        <!-- @foreach($city as $ct)
          <option value={{$ct->id}}> {{$ct->city_name}} </option>
        @endforeach -->
      </select>
    </div> <!-- form-group end.// -->
  </div> <!-- form-row.// -->
  <div class="form-group">
    <label>Create password</label>
      <input class="form-control" type="password" name="password" id="password">
  </div> <!-- form-group end.// --> 

  <div class="form-group">
    <label>Confirm password</label>
      <input class="form-control" type="password" name="cpassword" id="cpassword">
  </div>

  <div class="form-group">
    <label>Upload Profile Picture</label>
      <input class="form-control" type="file" name="profile" id="profile">
  </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> Register  </button>
    </div> <!-- form-group// -->      
    <small class="text-muted">By clicking the 'Sign Up' button, you confirm that you accept our <br> Terms of use and Privacy Policy.</small>                                          
</form>
</article> <!-- card-body end .// -->
<div class="border-top card-body text-center">Have an account? <a href="/login">Log In</a></div>
</div> <!-- card.// -->
</div> <!-- col.//-->

</div> <!-- row.//-->


</div> 
<br><br>
<!--container end.//-->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
  $(document).ready(function(){
      $.ajaxSetup(
            {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }
        );
  $('#registerFrm').validate({
                rules: {
                  fname: {
                    required: true,
                    minlength:3,
                    maxlength:50
                  },
                  lname:{
                    minlength:3,
                    maxlength:50,
                    required:true
                  },
                  email:{
                    required:true,
                    maxlength:50,
                    email:true
                  },
                  mobileno:{
                    required:true,
                    number:true,
                    minlength:10,
                    maxlength:12
                  },
                  state:{
                    required:true
                  },
                  city:{
                    required:true
                  },
                  password:{
                    required:true,
                    minlength : 5
                  },
                  cpassword:{
                    minlength : 5,
                    equalTo : "#password"
                  },
                  profile:{
                    required:true
                  }
                },  
                  messages: {
                  fname: {
                    required: "Please Enter First Name."
                  },
                  lname: {
                    required: "Please Enter Last Name."
                  },
                  email:{
                    required: "Please enter Email."
                  },
                  mobileno:{
                    required: "Please enter Mobile No."
                  },
                  state:{
                    required: "Please Select State"
                  },
                  city:{
                    required: "Please Select City"
                  },
                  password:{
                    required: "Please enter Password"
                  }
                },  

                submitHandler: function (form) {
                  var formData = new FormData($('#registerFrm')[0]);
                  var input = document.getElementById("profile");
                  file = input.files[0];
                  formData.append("profile", file);

                       $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        cache: false,
                        data: formData,
                        url: "add_register",
                              success: function(data)
                              {
                                if(data == 1){
                                  alert("Registerd successfully...");
                                }else{
                                  alert("something went please try again...");
                                }
                                
                              }
                        });
                }
});

  
  $(document).on('change','#state',function(){
    var state_val = $('#state').val();
      $.ajax({
                        type: "POST",
                        data: {
                          state:state_val
                        },
                        url: "get_city",
                              success: function(data)
                              {
                                /*var selOpts = "";
                                $.each(data, function(k, v)
                                {
                                  var id = data[k].id;
                                  var val = data[k].city_name;
                                  selOpts += "<option value='"+id+"'>"+val+"</option>";
                                });*/
                                $('#city').html(data);
                              }
                        });
  });

});
</script>
<style type="text/css">
  .error{
    color: red;
  }
</style>