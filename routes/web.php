<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
 
Route::get('register', 'RegisterController@register');
Route::post('add_register', 'RegisterController@add_register');
Route::post('get_city', 'RegisterController@get_city');
Route::get('login', 'LoginController@login');
Route::post('process_login', 'LoginController@process_login');

Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

	Route::any('home', 'HomeController@home');

});
